package com.innominds.tinder;

/**
 * This is the implementation of the UserCollections class
 * for the Dating Application
 * @author jpalepally1
 */

import java.util.ArrayList;
import java.util.Arrays;

public class UserCollections {

	ArrayList<User> listOfAllUsers = new ArrayList<User>();

	/**
	 * This is a method for creating a new user
	 * 
	 * @param name
	 * @param age
	 * @param gender
	 * @param height
	 * @param weight
	 * @param profile
	 * @param salary
	 * @param marital_Status
	 * @param food_Preference
	 * @param location
	 * @param primary_Hobby
	 * @param hobbies
	 * @param fav_Food
	 * @param food_Items
	 * @param fav_place
	 * @return
	 */
	public boolean createUser(String name, int age, String gender, float height, int weight, String profile, int salary,
			String marital_Status, String food_Preference, String location, String primary_Hobby, String[] hobbies,
			String fav_Food, String[] food_Items, String favourite_place, String[] fav_places) {
		User newUser = new User(name, age, gender, height, weight, profile, salary, marital_Status, food_Preference,
				location, primary_Hobby, hobbies, fav_Food, food_Items,favourite_place,fav_places);
		this.listOfAllUsers.add(newUser);
		return true;
	}

	/**
	 * This is a method to perform the name matching
	 * 
	 * @param string
	 * @return
	 */
	public ArrayList<User> getMatchName(String string) {
		ArrayList<User> set = new ArrayList<User>();
		System.out.println("                 Matched Names list               ");
		System.out.println("======================================================");
		for (User currUser : listOfAllUsers) {

			if (currUser.name == string)
				set.add(currUser);

		}

		return set;
	}

	/**
	 * This is a method to perform the location matching
	 * 
	 * @param loc
	 * @return
	 */
	public ArrayList<User> getMatchLocation(String loc) {
		ArrayList<User> set = new ArrayList<User>();
		System.out.println("                 Matched Locations list               ");
		System.out.println("======================================================");
		for (User currUser : listOfAllUsers) {

			if (currUser.location == loc)
				set.add(currUser);

		}

		return set;
	}

	/**
	 * This is a method to perform the profile matching
	 * 
	 * @param profile
	 * @return
	 */
	public ArrayList<User> getMatchProfile(String profile) {
		ArrayList<User> set = new ArrayList<User>();
		System.out.println("                 Matched Profiles list               ");
		System.out.println("======================================================");
		for (User currUser : listOfAllUsers) {

			if (currUser.profile == profile)
				set.add(currUser);

		}

		return set;
	}

	/**
	 * This is a method to perform the Primary_hobby matching
	 * 
	 * @param hobby
	 * @return
	 */
	public ArrayList<User> getMatchPrimary_Hobby(String hobby) {
		ArrayList<User> set = new ArrayList<User>();
		System.out.println("                 Matched Primary_Hobby list               ");
		System.out.println("======================================================");
		for (User currUser : listOfAllUsers) {

			if (currUser.primary_Hobby == hobby)
				set.add(currUser);

		}
		return set;

	}

	/**
	 * This is a method to perform the hobby OR matching
	 * 
	 * @param playthings
	 * @return
	 */
	public ArrayList<User> getMatchHobbies(String[] hobbyOR) {
		ArrayList<User> getHobbyORlist = new ArrayList<User>();
		System.out.println("                 Matched Hobbies OR list               ");
		System.out.println("======================================================");
		for (int i = 0; i < this.listOfAllUsers.size(); i++) {
			User newuserfound = this.listOfAllUsers.get(i);
			for (int j = 0; j < newuserfound.hobbies.length; j++) {
				for (int k = 0; k < hobbyOR.length; k++) {
					if (newuserfound.hobbies[j] == hobbyOR[k]) {
						getHobbyORlist.add(newuserfound);
					}
				}
			}
		}
		return getHobbyORlist;
	}

	/**
	 * This is a method to perform the hobby AND matching
	 * 
	 * @param hobbyAnd
	 * @return
	 */
	public ArrayList<User> getHobbyAndList(String[] hobbyAnd) {
		ArrayList<User> getHobbyAndlist = new ArrayList<User>();
		System.out.println("                 Matched Hobbies AND list               ");
		System.out.println("======================================================");
		User newuser = null;
		String hobby1 = hobbyAnd[0];
		String hobby2 = hobbyAnd[1];
		for (User currentuser : this.listOfAllUsers) {
			if ((Arrays.asList(currentuser.hobbies).contains(hobby1)
					&& (Arrays.asList(currentuser.hobbies).contains(hobby2)))) {
				newuser = currentuser;
				getHobbyAndlist.add(newuser);
			}
		}

		return getHobbyAndlist;
	}

	/**
	 * This is a method to perform the food_Preference either veg or non veg
	 * 
	 * @param preference
	 * @return
	 */
	public ArrayList<User> getMatch_Food_Preference(String preference) {
		ArrayList<User> set = new ArrayList<User>();
		System.out.println("                 Matched Food_Preference list               ");
		System.out.println("======================================================");
		for (User currUser : listOfAllUsers) {

			if (currUser.food_Preference == preference)
				set.add(currUser);

		}

		return set;

	}

	/**
	 * This is a method to perform the favorite food
	 * 
	 * @param favourite_Food
	 * @return
	 */

	public ArrayList<User> getMatch_Favourite_Food(String favourite_Food) {
		ArrayList<User> set = new ArrayList<User>();
		System.out.println("                 Matched Favourite_Food list               ");
		System.out.println("======================================================");
		for (User currUser : listOfAllUsers) {

			if (currUser.primary_Food == favourite_Food)
				set.add(currUser);

		}
		return set;

	}

	/**
	 * This is a method to perform the hobby OR matching
	 * 
	 * @param foodOR
	 * @return
	 */
	public ArrayList<User> getMatchFoodOR(String[] foodOR) {
		ArrayList<User> getFoodORlist = new ArrayList<User>();
		System.out.println("                 Matched Food OR list               ");
		System.out.println("======================================================");
		for (int i = 0; i < this.listOfAllUsers.size(); i++) {
			User newuserfound = this.listOfAllUsers.get(i);
			for (int j = 0; j < newuserfound.interest_Food_Items.length; j++) {
				for (int k = 0; k < foodOR.length; k++) {
					if (newuserfound.interest_Food_Items[j] == foodOR[k]) {
						getFoodORlist.add(newuserfound);
					}
				}
			}
		}
		return getFoodORlist;
	}

	/**
	 * This is a method to perform the Hobby AND Matching
	 * 
	 * @param foodAND
	 * @return
	 */

	public ArrayList<User> getMatchFoodAND(String[] foodAND) {
		ArrayList<User> getFoodAndlist = new ArrayList<User>();
		System.out.println("                 Matched Food AND list               ");
		System.out.println("======================================================");
		User newuser = null;
		String food1 = foodAND[0];
		String food2 = foodAND[1];
		for (User currentuser : this.listOfAllUsers) {
			if ((Arrays.asList(currentuser.interest_Food_Items).contains(food1)
					&& (Arrays.asList(currentuser.interest_Food_Items).contains(food2)))) {
				newuser = currentuser;
				getFoodAndlist.add(newuser);
			}
		}

		return getFoodAndlist;
	}

	public ArrayList<User> getMatchminsalary(int sal) {
		ArrayList<User> set = new ArrayList<User>();
		System.out.println("                 Matched Min Salary list               ");
		System.out.println("======================================================");
		for (User currUser : listOfAllUsers) {

			if (currUser.salary >= sal)
				set.add(currUser);

		}
		return set;

	}

	public ArrayList<User> getMatchmaxsalary(int sal) {
		ArrayList<User> set = new ArrayList<User>();
		System.out.println("                 Matched Max Salary list                  ");
		System.out.println("==========================================================");
		for (User currUser : listOfAllUsers) {

			if (currUser.salary <= sal)
				set.add(currUser);
		}
		return set;

	}

	public ArrayList<User> getMatchminage(int age) {
		ArrayList<User> set = new ArrayList<User>();
		System.out.println("				 Matched Min Age 			             ");
		System.out.println("=========================================================");
		for (User currUser : listOfAllUsers) {

			if (currUser.age >= age)
				set.add(currUser);
		}
		return set;

	}

	public ArrayList<User> getMatchmaxage(int age) {
		ArrayList<User> set = new ArrayList<User>();
		System.out.println("               Matched Max age                          ");
		System.out.println("=========================================================");
		for (User currUser : listOfAllUsers) {

			if (currUser.age <= age)
				set.add(currUser);
		}
		return set;
	}

	public ArrayList<User> getMatchminheight(float Float) {
		ArrayList<User> set = new ArrayList<User>();
		System.out.println("               Matched Max Height                         ");
		System.out.println("=========================================================");
		for (User currUser : listOfAllUsers) {

			if (currUser.height >= Float)
				set.add(currUser);
		}
		return set;
	}

	public ArrayList<User> getMatchmaxheight(float Float) {
		ArrayList<User> set = new ArrayList<User>();
		System.out.println("               Matched Max Height                         ");
		System.out.println("=========================================================");
		for (User currUser : listOfAllUsers) {

			if (currUser.height <= Float)
				set.add(currUser);
		}
		return set;
	}

	public ArrayList<User> getMatchmaxweight(int weight) {
		ArrayList<User> set = new ArrayList<User>();
		System.out.println("               Matched Min Weight                         ");
		System.out.println("=========================================================");
		for (User currUser : listOfAllUsers) {

			if (currUser.weight <= weight)
				set.add(currUser);
		}
		return set;
	}

	public ArrayList<User> getMatchminweight(int weight) {
		ArrayList<User> set = new ArrayList<User>();
		System.out.println("               Matched Max Weight                         ");
		System.out.println("=========================================================");
		for (User currUser : listOfAllUsers) {

			if (currUser.weight >= weight)
				set.add(currUser);
		}
		return set;
	}

	public ArrayList<User> getMatchrangeofweight(int minweight, int maxweight) {
		ArrayList<User> set = new ArrayList<User>();
		System.out.println("               Matched Range of  Weight                         ");
		System.out.println("=========================================================");
		for (User currUser : listOfAllUsers) {

			if (currUser.weight > minweight && currUser.weight < maxweight) {
				set.add(currUser);
			}
		}
		return set;
	}

	public ArrayList<User> getMatchrangeofage(int minage, int maxage) {
		ArrayList<User> set = new ArrayList<User>();
		System.out.println("               Matched Range of  age                          ");
		System.out.println("=========================================================");
		for (User currUser : listOfAllUsers) {

			if (currUser.age > minage && currUser.age < maxage) {
				set.add(currUser);
			}
		}
		return set;
	}

	public ArrayList<User> getMatchrangeofheight(Float minheight, Float maxheight) {
		ArrayList<User> set = new ArrayList<User>();
		System.out.println("               Matched Range of  height                          ");
		System.out.println("=========================================================");
		for (User currUser : listOfAllUsers) {

			if (currUser.height > minheight && currUser.height < maxheight) {
				set.add(currUser);
			}
		}
		return set;
	}
	
	public ArrayList<User> getMatchfavplace(String favourite_place) {
		ArrayList<User> set = new ArrayList<User>();
		System.out.println("               Matched Favorite places                   ");
		System.out.println("=========================================================");
		for (User currUser : listOfAllUsers) {

			if (currUser.favourite_place == favourite_place ) {
				set.add(currUser);
			}
		}
		return set;
	}
}

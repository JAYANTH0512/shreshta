package Hello;

/**
 * This is a class containing methods implementing fibonacciSeries.
 * @author jpalepally1
 */
public class Fibonacciseries {
	
	public static void main(String[]args){
		//initialize and declare the variables and count
		
		int n = 3; // nth FibonacciSeries  is to be calculated
		
		try {
			Fibonacciseries fs = new Fibonacciseries();
			int nthFibonacciNumber = fs.getNthFibonacci(n);
			System.out.println("The " + n + "th fibonacci number is " + nthFibonacciNumber);	
		} catch (Throwable th) {
			System.out.println("The input number " + n + " is a negative number.");
		}
	}

	/**
	 * 
	 * @param n nth FibonacciSeries will be calculated
	 * @return the nth FibonacciSeries
	 * @throws Throwable
	 */
	int getNthFibonacci(int n) throws Throwable {

		if (n <= 0) {
			throw new Throwable();
		} else {
			if (n==1) {
				return 1;
			} else if (n == 2) {
				return 1;
			} else {
				int term1 = 1, term2 = 1;
				int term3 = 0;
				for (int i=0; i < (n-2); i++) {
					term3 = term1 + term2;
					term1 = term2;
					term2 = term3;
				}
				return term3;
			}
		}
	}
}
package Hello;

/**
 * Class containing methods to check prime
 * @author jpalepally1
 */
public class PrimeNumCheck {
	
	// main method
	public static void main(String[] args) {
		// declare and initialize the variables
		int number = 5;

		try {
			PrimeNumCheck myFirstObject = new PrimeNumCheck();
			boolean isPrime = myFirstObject.isNumberPrime(number);
			System.out.println("The number you have provided " + number 
					+ (isPrime ? " is " : " is not") + " a prime number.");

		} catch (Throwable e) {
			System.out.println("The nubmer you have provided " + number + " is invalid. Use only positive integers.");
		}
	}

	/**
	 * This is an instance method, to calculate whether the input is a prime or not.
	 * 
	 * @param inputNumber
	 * @return true if the input number is a prime, or return false
	 * @throws Throwable 'Exception' exception if input itself is invalid
	 */
	public boolean isNumberPrime(int inputNumber) throws Throwable {
		int count = 0;
		if (inputNumber < 2) {			// checking if number is in valid range
			throw new Exception();
		} else {
			for (int i = 1; i <= inputNumber; i++) {
				if (inputNumber % i == 0)
					count++;
			}
		}

		return (count == 2) ? true : false;
	}
}
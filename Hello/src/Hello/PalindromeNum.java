package Hello;
/**
 * `PalindromeNumber is to reverse that number we get the same number
 * @author jpalepally1
 *
 */
public class PalindromeNum {
	
	public static void main(String[]args){
		//declare variables and initialize values for sum and number
		int userInput =121;
		
		try {
			PalindromeNum newPalindromeNumber = new PalindromeNum();
			boolean isItAPalindrome = newPalindromeNumber.isPalindrome(userInput);
			
			System.out.print("Input number " + userInput + " is ");
			if (!isItAPalindrome) {
				System.out.println("not ");
			}
			System.out.println("a palindrome.");
			
		} catch (Throwable th) {
			System.out.println("Input is invalid, " + userInput);
		}
		
	}
	
	/**
	 * Method to check whether inputNumber is a PalindromeNumber.
	 * @param inputNumber input to check 
	 * @return true if input number is a PalindromeNumber, else false
	 */
	boolean isPalindrome(int inputNumber) throws Throwable {
		
		if (inputNumber < 0) {
			throw new Throwable();
		}
		
		int temp = inputNumber;
		int rem, sum = 0;
		//check the condition if the condition is true it will enter into the loop 
		while(inputNumber>0){
			//getting remainder
			rem = inputNumber%10;
			sum = (sum*10)+rem;
			inputNumber=inputNumber/10;
		}

		return (temp == sum) ? true : false;
	}
}
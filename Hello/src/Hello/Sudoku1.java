package Hello;
/**
 * Write a code for sudoku1  puzzle
 * @author jpalepally1
 *
 */
public class Sudoku1 {
	
		
		public static int[][] Grid_To_Solve= {
				                              {1,2,0,3},
				                              {2,0,1,0},
				                              {0,0,0,0},
				                              {4,0,0,0},
		};
		public int[][] board;
		public static final int Empty = 0; // empty cell
		public static final int Size = 4; // size of our Sudoku1 grids
		
		public void Sudoku(int[][] board) {
			this.board = new int[4][4];
		
			for (int i = 0; i < 4; i++) {
				for (int d = 0; d < 4; d++) {
					this.board[i][d] = board[i][d];	
				}		
	  }
			}
		
//Check if the number is in row
public boolean isInRow(int row, int number )
{ 
	for(int i=0; i<4;i++)
	    if(board[row][i]==number)
	       return true;
	    return  false;
}

//Check if the number is in column
public boolean isInCol(int col,int number)
{ 
	for(int d=0; d<4;d++)
	    if(board[col][d]==number)
	       return true;
	    return  false;
	    }
}


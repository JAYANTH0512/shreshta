package Hello;

/**
 * The BinaryMathLibrary is a collection of methods
 * to demonstrate the binary system and arithmetic
 * operations on the binary system. 
 * @author Puran
 *
 */
public class BinaryMathLibrary {
	
	public static void main (String args[]) {
		BinaryNumber bn = new BinaryNumber(22.234f);
		System.out.println(bn.toString());
	}
}

class BinaryNumber {
	int[] num;
	
	public BinaryNumber(float inputFloat) {
		num = new int[16];
		
		// get whole part
		int wholePart = (int) inputFloat;
		System.out.println("Whole part is " + wholePart);
		System.out.println("Binary="+Integer.toBinaryString(wholePart));
		
		float fractionalPartFraction = inputFloat - wholePart;
		System.out.println("fractionalPartFraction is " + fractionalPartFraction);
		
		// convert whole part to binary and update "num"
		
		// convert fractional part to binary and update "num"
	}
	
	public String toString() {
		String outputString = "";
		for (int i=0; i<16; i++) {
			outputString += num[i];
		}
		return outputString;
	}
}
/**
 * Modeling of Customer entity of
 * flipkart
 * 
 */
package Hello;

public class  Customer {
	int idno;
	String firstname;
	String lastname;
	String email;
	String createpassword;
	String confirmpassword;

	Customer()
	{
		
	}
// Constructor	
Customer(int idno,String lastname,String email,String createpassword,String confirmpassword, String firstname)
{
	this.idno = idno;
	this.firstname = firstname;
	this.lastname = lastname;
	this.email = email;
	this.createpassword = createpassword;
	this.confirmpassword = confirmpassword;
}
//Getter and setter Method
public int getIdno() {
	return idno;
}
public void setIdno(int idno) {
	this.idno = idno;
}
public String getFirstname() {
	return firstname;
}
public void setFirstname(String firstname) {
	this.firstname = firstname;
}
public String getLastname() {
	return lastname;
}
public void setLastname(String lastname) {
	this.lastname = lastname;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getCreatepassword() {
	return createpassword;
}
public void setCreatepassword(String createpassword) {
	this.createpassword = createpassword;
}
public String getConfirmpassword() {
	return confirmpassword;
}
public void setConfirmpassword(String confirmpassword) {
	this.confirmpassword = confirmpassword;
}
@Override
public String toString() {
	return "Signup [idno=" + idno + ", firstname=" + firstname + ", lastname=" + lastname + ", email=" + email
			+ ", createpassword=" + createpassword + ", confirmpassword=" + confirmpassword + "]";
}

}
package Hello;
/**
 * Swapping of two numbers by using temporary variable
 * 
 * @author jpalepally
 *
 */

public class Swapping {
	public static void main(String[] args) {
		//Declaring and initializing the varoables of swapno1 and swapno2
		int swapno1=10;
		int swapno2=20;
		int tempno;
		System.out.println("Before swapping \n"+swapno1+ "\n" +swapno2);
		
		//After swapping
		tempno=swapno1;
		swapno1=swapno2;
		swapno2=tempno;
		System.out.println("After swapping \n"+swapno1+ "\n" +swapno2);				
	}
}

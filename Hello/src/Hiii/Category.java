package Hiii;
/**
 * Declaring The Datatypes
 * for category
 * @author jpalepally
 *
 */
public class Category {
	private int Id;
	private String name;
	Category() {     // Object is created in Main.java
		 
	}
	Category(int Id,String name){
		
	}
	public int getId() {   // Using Getters and setters method
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String toString() {
		return "Category [Id=" + Id+ ", name=" + name + "]";
	
	}
}


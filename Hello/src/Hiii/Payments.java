package Hiii;
/**
 * Declaring the datatypes and datavariables
 * for Payments
 * @author jpalepally
 *
 */

public class Payments {
	private String paymentId;
	private String Modeofpayment;
	private String status;
	
	Payments()  // Method creation
	{
		
	}
	
 Payments(String paymentId,String Modeofpayment,String status)
 {
	 this.paymentId = paymentId;
	 this.Modeofpayment = Modeofpayment;
	 this.status = status;
 }

public String getPaymentId() {  // Getters and setters method
	return paymentId;
}

public void setPaymentId(String paymentId) {
	this.paymentId = paymentId;
}

public String getModeofpayment() {
	return Modeofpayment;
}

public void setModeofpayment(String modeofpayment) {
	Modeofpayment = modeofpayment;
}

public String getStatus() {
	return status;
}

public void setStatus(String status) {
	this.status = status;
}

@Override
public String toString() {
	return "Payments [paymentId=" + paymentId + ", Modeofpayment=" + Modeofpayment + ", status=" + status + "]";
}

}
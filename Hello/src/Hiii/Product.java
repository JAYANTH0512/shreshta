package Hiii;
/**
 * Declaring the Datatypes for
 * products
 * @author jpalepally
 *
 */




public class Product{
	  private String quality;
	  private String cost;
	  private int Rating;
	  private double Discount;
	  
	  Product()// Creating the method for Product
	  {
		  
	  }

	  Product(String quality,String cost,int Id,int Rating,int Discount)
	  {
			this.quality = quality;
			this.cost = cost;
		}
		public String getquality() {   // Using Getters and setters Method
			return quality;
		}
		public void setquality(String quality) {
			this.quality = quality;
		}
		public String getcost() {
			return cost;
		}
		public void setcost(String cost) {
			this.cost = cost;
		}
		
		public int getRating() {
			return Rating;
		}
		public void setRating(int Rating) {
			this.Rating = Rating;
		}
		public double getDiscount() {
			return Discount;
		}
		public void setDiscount(double Discount) {
			this.Discount = Discount;
		}
		@Override
		public String toString() {
			return "Product [quality=" + quality + ", cost=" + cost + ",Rating=" + Rating+",Discount=" + Discount +"]";
		}
		
	}
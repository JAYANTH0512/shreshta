package Hiii;
/**
 * Declaring the Datatypes and datavariables for
 * Address
 * @author jpalepally
 *
 */

public class Address {
	private String houseno;
	private String street;
	private int pincode;
	private String Distict;
	private String State;
	private String Country;
	
	
	Address()//Creating a method 
	{
		
	}
	Address(String houseno,String street,int pincode,String Distict,String state,String Country, String State)
	{
		this.setHouseno(houseno);
		this.setStreet(street);
		this.setPincode(pincode);
		this.setDistict(Distict);
		this.setState(State);
		this.setCountry(Country);
		
		
	}
	/**
	 * @return the houseno
	 */
	public String getHouseno() {
		return houseno;
	}
	/**
	 * @param houseno the houseno to set
	 */
	public void setHouseno(String houseno) {
		this.houseno = houseno;
	}
	/**
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}
	/**
	 * @param street the street to set
	 */
	public void setStreet(String street) {
		this.street = street;
	}
	/**
	 * @return the pincode
	 */
	public int getPincode() {
		return pincode;
	}
	/**
	 * @param pincode the pincode to set
	 */
	public void setPincode(int pincode) {
		this.pincode = pincode;
	}
	/**
	 * @return the distict
	 */
	public String getDistict() {
		return Distict;
	}
	/**
	 * @param distict the distict to set
	 */
	public void setDistict(String distict) {
		Distict = distict;
	}
	/**
	 * @return the state
	 */
	public String getState() {
		return State;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		State = state;
	}
	/**
	 * @return the country
	 */
	public String getCountry() {
		return Country;
	}
	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		Country = country;
	}
	
		
	}
	
	
	
	
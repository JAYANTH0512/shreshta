package com.innominds.tinder;
/**
 * This class Represents the enum of Gender
 * @author jpalepally1
 *
 */
public enum Gender {
	Male,
	Female,
	Others;
}

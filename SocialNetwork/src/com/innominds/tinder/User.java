package com.innominds.tinder;

/**
 * This is the implementation of User class for the Dating Application
 * 
 * @author jpalepally1
 */

//import java.util.ArrayList;

public class User {

	public String name;
	public int age;
	public String profile;
	public String primary_Hobby;
	public String[] hobbies;
	public float height;
	public int weight;
	public int salary;
	public String food_Preference;
	public String primary_Food;
	public String[] interest_Food_Items;
	public String location;
	public String favourite_place;
	public String[] fav_places;
	Gender gender_type;
	Maritalstatus status_type;
	Qualification[] qual;
	/**
	 * create a method i.e createNewBlunderUser
	 * 
	 * @param profileName
	 * @param age
	 * @param designation
	 * @param hobby
	 * @return
	 */
	

	public User(String name, int age, Gender gender_type, float height, int weight, String profile, int salary,
			Maritalstatus status_type, String food_Preference, String location, String primary_Hobby, String[] hobbies,
			String fav_Food, String[] food_Items, String favourite_place, String[] fav_places,Qualification[] qual) {
		this.name				 = name;
		this.age 			 	 = age;
		this.profile 		 	 = profile;
		this.gender_type		 = gender_type;
		this.height			 	 = height;
		this.weight			 	 = weight;
		this.salary			 	 = salary;
		this.status_type 	     = status_type;
		this.food_Preference 	 = food_Preference;
		this.location 		 	 = location;
		this.primary_Hobby	 	 = primary_Hobby;
		this.hobbies		 	 = hobbies;
		this.primary_Food 	 	 = fav_Food;
		this.interest_Food_Items = food_Items;
		this.favourite_place 	 = favourite_place;
		this.fav_places 		 = fav_places;
		this.qual=qual;

		System.out.println(this);
	}

	/**
	 * Overriding the default toString method of User so that User can be printed in
	 * human readable format.
	 */
	public String toString() {

		String stringToReturn = "";

		stringToReturn += "               Display the All user details             \n";
		stringToReturn += "==================================================" + "\n";
		stringToReturn += "name                        :                " + this.name + "\n";
		stringToReturn += "Age                         :                " + this.age + "\n";
		stringToReturn += "Gender                      :                " + this.gender_type + "\n";
		stringToReturn += "Height                      :                " + this.height + "\n";
		stringToReturn += "Weight                      :                " + this.weight + "\n";
		stringToReturn += "Salary                      :                " + this.salary + "\n";
		stringToReturn += "Marital_Status              :                " + this.status_type + "\n";
		stringToReturn += "Food_Preference             :                " + this.food_Preference + "\n";
		stringToReturn += "Location                    :                " + this.location + "\n";
		stringToReturn += "Primary_Hobby               :                " + this.primary_Hobby + "\n";
		stringToReturn += "Primary_Food                :                " + this.primary_Food + "\n";
		stringToReturn += "Favourite_Place             :                " + this.favourite_place + "\n";

		String hobbyString = "[";
		for (int i = 0; i < this.hobbies.length; i++) {
			hobbyString += "\"" + this.hobbies[i] + "\",";
		}
		hobbyString += "]";

		stringToReturn += "Hobbies are                 :                " + hobbyString + "\n";
		
		String qualificationString = "[";
		for (int i = 0; i < this.qual.length; i++) {
			qualificationString += "\"" + this.qual[i] + "\",";
		}
		qualificationString += "]";

		stringToReturn += qualificationString + "\n";
		
		

		String foodString = "[";
		for (int i = 0; i < this.interest_Food_Items.length; i++) {
			foodString += "\"" + this.interest_Food_Items[i] + "\",";
		}
		foodString += "]";

		stringToReturn += "Food items are              :               " + foodString + "\n";

		String fav_placesString = "[";
		for (int i = 0; i < this.fav_places.length; i++) {
			fav_placesString += "\"" + this.fav_places[i] + "\",";

		}
		fav_placesString += "]";

		stringToReturn += "Fav_places are              :               " + fav_placesString + "\n";
		stringToReturn += "*************************" + "\n";
		System.out.println("\n");
		return stringToReturn;
	}

}

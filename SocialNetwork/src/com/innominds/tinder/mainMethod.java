package com.innominds.tinder;

/**
 This is the Main method for the Dating Application
 * @author jpalepally1
 *
 */

import java.util.ArrayList;
import java.util.Scanner;

public class mainMethod {
	public static void main(String[] args) {

		@SuppressWarnings("resource")
		Scanner scn = new Scanner(System.in);
		// Creating a object
		UserCollections u = new UserCollections();
		
		
		Qualification q1=new Qualification("Btech","Cse","MRiT","757","2021","4");
		Qualification q2=new Qualification("Mtech","Civil","Ou","824","2022","3");
		Qualification q3=new Qualification("bsc","mech","Aurora","858","2016","3");
		Qualification q4=new Qualification("Msc","mech","Layola","815","2019","2");
		Qualification q5=new Qualification("Diploma","mech","JNGPT","780","2017","3");
		
		
		Gender g1=Gender.Male;
		Gender g2=Gender.Female;
		Gender g3=Gender.Others;
		
		Maritalstatus M1 = Maritalstatus.MARRIED;
		Maritalstatus M2 = Maritalstatus.SINGLE;
		Maritalstatus M3 = Maritalstatus.NOTINTERESTED;
		
		u.createUser("Jai", 23, g1, 5.9F, 65, "Software Trainee Engineer", 60000,M1,  "Non-veg", "Hyderabad","Cricket",
				new String[] { "MemeCreator", "Cricket", "Guitarist" }, "Chicken",
				new String[] { "rice", "chapathi", "chicken" }, "Vizag",
				new String[] { "Coimbatore", "Switzarland", "Araku"},new Qualification[] {q1});
				
		u.createUser("Manu", 23, g3, 5.8F, 55, "Senior Software Engineer", 90000,M2,  "Non-veg","Hyderabad", "Coding", 
				new String[] { "Chatting", "Coding", "Talking" }, "Chapathi",
				new String[] { "Bhajji", "mutton", "dosa" }, "Hyderabad",
				new String[] { "Tirupathi", "Manali", "Srinagar" },new Qualification[] {q2});
		u.createUser("Rohini", 24, g2, 5.8F, 50, "Drug controller", 70000,M2,  "veg", "Rajapet", "Reading",
				new String[] { "Reading", "Travelling", "Teaching" }, "Dosa",
				new String[] { "Rice", "Panner", "Vada" },"Ananthagiri Hills",
				new String[] { "Kodaikanal", "Ooty", "Mumbai" },new Qualification[]{q4});
		u.createUser("Ruchitha", 22,g2, 5.7F, 45, "Support Manager", 25000,M2, "veg", "Rajapet","Taking Selfies",
				new String[] { "Reading", "Chatting", "Talking" }, " Kaju-Biryani",
				new String[] { "Pani-Puri", "Ice-Cream", "dosa" }, "Chennai",
				new String[] { "Chennai Beach", "Mahabalipuram", "Bulgaria" },new Qualification[] {q1});
		u.createUser("Anil", 23, g1, 5.8F, 63, "Software Trainee Engineer", 30000,M2,  "veg", "Vizag", "Watching",
				new String[] { "Watching", "Reading", "Teaching" }, "Puri",
				new String[] { "LegPiece", "Biryani", "Bonda" }, "Vizag",
				new String[] { "Kailasagiri", "Rushikonda Beach", "Araku" },new Qualification[] {q1});
		u.createUser("Sravani", 22, g2, 5.1F, 58, "Java developer", 40000,M3,  "Non-veg", "Tuni", "Planning",
				new String[] { "Drawing", "Coding", "Playing" }, "Chicken",
				new String[] { "Panipuri", "Eggbajji", "parotta" }, "Rameshwaram",
				new String[] { "Tirupathi", "Srisailam", "Varanasi" },new Qualification[] {q4});
		u.createUser("Ashok", 23, g1 ,5.6F, 64, "Data analyst", 50000,M2,  "Non-veg", "Adigoppula", "Coding",
				new String[] { "Laughing", "Playing", "Training" }, "Bhajji",
				new String[] { "Manchuria", "ChickenPakodi", "Noodles" }, "Vizag",
				new String[] { "RK Beach", "Kailasagiri", "Araku" },new Qualification[] {q5});
		u.createUser("Swapna", 22, g2, 5.2F, 59, "Java developer", 60000,M2,  "Non-veg", "Kamareddy", "Playing",
				new String[] { "Eating", "Talking", "Walking" }, "Biryani",
				new String[] { "LegPiece", "Biryani", "Bonda" }, "Nizamabad",
				new String[] { "Karimnagar", "Warangal", "Texas" },new Qualification[] {q2});
		u.createUser("Kiran", 23, g1, 5.8F, 64, "Senior Frontend developer", 70000,M3,  "Non-veg", "Narsapur","Coding",
				new String[] { "Playing", "Running", "Drinking" }, "Biryani",
				new String[] { "rice", "chapathi", "chicken" }, "Narsapur",
				new String[] { "Kakinada", "Gaya", "Ayodhya" },new Qualification[] {q3});
		u.createUser("Ashok", 27, g1, 5.4F, 70, "Senior Software Engineer", 80000,M1,  "Non-veg", "Bihar","Cricket",
				new String[] { "Watching", "Jogging", "Teaching" }, "Friedrice",
				new String[] { "Bhajji", "mutton", "dosa" }, "Araku",
				new String[] { "Hyderabad", "Kurnool", "Tirupathi" },new Qualification[] {q5});

		System.out.println("1.Name");
		System.out.println("2.Location");
		System.out.println("3.Profile");
		System.out.println("4.PrimaryHobbie");
		System.out.println("5.MatchHobbiesOR");
		System.out.println("6.MatchHobbiesAnd");
		System.out.println("7.FoodPreference");
		System.out.println("8.Favouritefood");
		System.out.println("9.Food Or");
		System.out.println("10.Food And");
		System.out.println("11.Salary Min");
		System.out.println("12.Salary Max");
		System.out.println("13.Minimum age");
		System.out.println("14.Maximum age");
		System.out.println("15.Minimum Height");
		System.out.println("16.Maximum Height");
		System.out.println("17.Minimum Weight");
		System.out.println("18.Maximum Weight");
		System.out.println("19.Range of Weight");
		System.out.println("20.Range of age");
		System.out.println("21.Range of height");
		System.out.println("22.Favourite place");
		System.out.println("23.Favourite place OR");
		System.out.println("24.Gender");
		System.out.println("25.Favourite place And");
		System.out.println("26.Marital status");
		System.out.println("27.User Qualification");
		
		int n = scn.nextInt();
		switch (n) {

		case 1:
			// create a method for searching the names
			ArrayList<User> res1 = u.getMatchName("Ashok");
			System.out.println(res1);
			System.out.println(" Total Names matched:" + res1.size());
			break;

		case 2:
			// create a method for searching the location
			ArrayList<User> res2 = u.getMatchLocation("Guntur");
			System.out.println(res2);
			System.out.println("Total Locations Matched:" + res2.size());
			break;

		case 3:
			// create a method for searching the profile
			ArrayList<User> res3 = u.getMatchProfile("Java developer");
			System.out.println(res3);
			System.out.println("Total Profiles Matched:" + res3.size());
			break;

		case 4:
			// create a method for searching the primary hobby

			ArrayList<User> res4 = u.getMatchPrimary_Hobby("Coding");
			System.out.println(res4);
			System.out.println("Total Primary_Hobbies Matched:" + res4.size());
			break;

		case 5:
			// create a method for searching the OR hobbies

			ArrayList<User> res5 = u.getMatchHobbies(new String[] { "Coding", "Laughing" });
			System.out.println(res5);
			System.out.println("Total OR hobbies Matched:" + res5.size());
			break;

		case 6:
			// create a method for searching the AND hobbies
			ArrayList<User> res6 = u.getHobbyAndList(new String[] { "Laughing", "Playing" });
			System.out.println(res6);
			System.out.println("Hobbies And List:" + res6.size());
			break;

		case 7:
			// create a method for searching the food_Preference

			ArrayList<User> res7 = u.getMatch_Food_Preference("veg");
			System.out.println(res7);
			System.out.println("Food preference list:" + res7.size());
			break;

		case 8:
			// create a method for searching the favorite food

			ArrayList<User> res8 = u.getMatch_Favourite_Food("Biryani");
			System.out.println(res8);
			System.out.println("Food preference list:" + res8.size());
			break;

		case 9:
			// create a method for searching the food OR list

			ArrayList<User> res9 = u.getMatchFoodOR(new String[] { "Biryani", "Fish" });
			System.out.println(res9);
			System.out.println("Food preference list:" + res9.size());
			break;

		case 10:
			// create a method for searching the food And list

			ArrayList<User> res10 = u.getMatchFoodAND(new String[] { "Manchuria", "Noodles" });
			System.out.println(res10);
			System.out.println("Food preference list:" + res10.size());
			break;

		case 11:
			// create a method for searching the Minimum Salary

			ArrayList<User> res11 = u.getMatchminsalary(60000);
			System.out.println(res11);
			System.out.println("salary preference list:" + res11.size());
			break;

		case 12:
			// create a method for searching the Maximum Salary

			ArrayList<User> res12 = u.getMatchmaxsalary(80000);
			System.out.println(res12);
			System.out.println("salary preference list:" + res12.size());
			break;
		case 13:
			// create a method for searching the people of age for minimum of 18

			ArrayList<User> res13 = u.getMatchminage(18);
			System.out.println(res13);
			System.out.println("age preference list:" + res13.size());
			break;

		case 14:
			// Create the method for searching the people for maximum age

			ArrayList<User> res14 = u.getMatchmaxage(27);
			System.out.println(res14);
			System.out.println("age preference list:" + res14.size());
			break;

		case 15:
			// Create the method for searching the people for minimum height

			ArrayList<User> res15 = u.getMatchminheight(5.2f);
			System.out.println(res15);
			System.out.println("height preference list:" + res15.size());
			break;

		case 16:
			// Create the method for searching the people for maximum height

			ArrayList<User> res16 = u.getMatchmaxheight(6.0f);
			System.out.println(res16);
			System.out.println("height preference list:" + res16.size());
			break;

		case 17:
			// Create the method for searching the people for Minimum Weight

			ArrayList<User> res17 = u.getMatchminweight(40);
			System.out.println(res17);
			System.out.println("weight preference list:" + res17.size());
			break;

		case 18:
			// Create the method for searching the people for Maximum Weight

			ArrayList<User> res18 = u.getMatchmaxweight(80);
			System.out.println(res18);
			System.out.println("weight preference list:" + res18.size());
			break;

		case 19:
			// Create the method for searching the people for Maximum Weight

			ArrayList<User> res19 = u.getMatchrangeofweight(40, 80);
			System.out.println(res19);
			System.out.println("weight preference list:" + res19.size());
			break;

		case 20:
			// Create the method for searching the people for Range of age

			ArrayList<User> res20 = u.getMatchrangeofage(18, 27);
			System.out.println(res20);
			System.out.println("Rangeofweight preference list:" + res20.size());
			break;

		case 21:
			// Create the method for searching the people for Range of height

			ArrayList<User> res21 = u.getMatchrangeofheight(5.2f, 6.0f);
			System.out.println(res21);
			System.out.println("Rangeofheight preference list:" + res21.size());
			break;

		case 22:
			// Create the method for searching the people for Favorite places

			ArrayList<User> res22 = u.getMatchfavplace("Vizag");
			System.out.println(res22);
			System.out.println("Favouriteplace preference list:" + res22.size());
			break;

		case 23:
			// Create the method for searching the people of favorite places OR
			
			ArrayList<User> res23 = u.getMatchFavplacesOR(new String[] { "Tirupathi", "Manali" });
			System.out.println(res23);
			System.out.println("Total fav places matched :" + res23.size());
			break;
			
		
		case 24:
			// Create the method for searching the people of Gender
		
			ArrayList<User> res24 = u.getMatchGender(g3);
			System.out.println(res24);
			System.out.println("Total Gender matched :" + res24.size());
			break;
			
		case 25:
			// Create the method for searching the people of favorite places And
			
			ArrayList<User> res25 = u.getMatchFavplacesAnd(new String[] { "Kailasagiri", "Araku" });
			System.out.println(res25);
			System.out.println("Total fav places And matched :" + res25.size());
			break;
			
		case 26:
			// Create the method for searching the people about their marital_status
		
			ArrayList<User> res26 = u.getMatchMaritalstatus(M1);
			System.out.println(res26);
			System.out.println("Total Marital_status matched :" + res26.size());
			break;
			
		case 27:
			// Create the method for searching the people about their Qualification
		
			ArrayList<User> res27 = u.getMatchQualification("vizag");
			System.out.println(res27);
			System.out.println("Total Qualification matched :" + res27.size());
			break;
		
	   }
    }
}

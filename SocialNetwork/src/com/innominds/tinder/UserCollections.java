package com.innominds.tinder;

/**
 * This is the implementation of the UserCollections class
 * for the Dating Application
 * @author jpalepally1
 */

import java.util.ArrayList;
import java.util.Arrays;
//import java.util.HashSet;
import java.util.HashSet;

public class UserCollections {

	ArrayList<User> listOfAllUsers = new ArrayList<User>();

	/**
	 * This is a method for creating a new user
	 * 
	 * @param name
	 * @param age
	 * @param gender
	 * @param height
	 * @param weight
	 * @param profile
	 * @param salary
	 * @param marital_Status
	 * @param food_Preference
	 * @param location
	 * @param primary_Hobby
	 * @param string
	 * @param string
	 * @param
	 * @param hobbies
	 * @param fav_Food
	 * @param food_Items
	 * @param fav_place
	 * @return
	 */
	public boolean createUser(String name, int age, Gender gender_type, float height, int weight, String profile,
			int salary, Maritalstatus status_type, String food_Preference, String location, String primary_Hobby,
			String[] hobbies, String fav_Food, String[] food_Items, String favourite_place, String[] fav_places,Qualification[] qual){
		User newUser = new User(name, age, gender_type, height, weight, profile, salary, status_type, food_Preference,
				location, primary_Hobby, hobbies, fav_Food, food_Items, favourite_place, fav_places,qual);
		this.listOfAllUsers.add(newUser);
		return true;

	}

	/**
	 * This is a method to perform the name matching
	 * of the user
	 * @param string
	 * @return
	 */
	public ArrayList<User> getMatchName(String string) {
		ArrayList<User> set = new ArrayList<User>();
		System.out.println("                 Matched Names list               ");
		System.out.println("======================================================");
		for (User currUser : listOfAllUsers) {

			if (currUser.name == string)
				set.add(currUser);

		}

		return set;
	}

	/**
	 * This is a method to perform the location matching
	 * of the user
	 * @param loc
	 * @return
	 */
	public ArrayList<User> getMatchLocation(String loc) {
		ArrayList<User> set = new ArrayList<User>();
		System.out.println("                 Matched Locations list               ");
		System.out.println("======================================================");
		for (User currUser : listOfAllUsers) {

			if (currUser.location == loc)
				set.add(currUser);

		}

		return set;
	}

	/**
	 * This is a method to perform the profile matching
	 * of the user
	 * @param profile
	 * @return
	 */
	public ArrayList<User> getMatchProfile(String profile) {
		ArrayList<User> set = new ArrayList<User>();
		System.out.println("                 Matched Profiles list               ");
		System.out.println("======================================================");
		for (User currUser : listOfAllUsers) {

			if (currUser.profile == profile)
				set.add(currUser);

		}

		return set;
	}

	/**
	 * This is a method to perform the Primary_hobby matching
	 * of the user
	 * @param hobby
	 * @return
	 */
	public ArrayList<User> getMatchPrimary_Hobby(String hobby) {
		ArrayList<User> set = new ArrayList<User>();
		System.out.println("                 Matched Primary_Hobby list               ");
		System.out.println("======================================================");
		for (User currUser : listOfAllUsers) {

			if (currUser.primary_Hobby == hobby)
				set.add(currUser);

		}
		return set;
	}

	/**
	 * This is a method to perform the hobby OR matching OR refers to the Match any
	 * one of the condition
	 * @param playthings
	 * @return
	 */
	public ArrayList<User> getMatchHobbies(String[] hobbyOR) {
		ArrayList<User> getHobbyORlist = new ArrayList<User>();
		System.out.println("                 Matched Hobbies OR list               ");
		System.out.println("======================================================");
		for (int i = 0; i < this.listOfAllUsers.size(); i++) {
			User newuserfound = this.listOfAllUsers.get(i);
			for (int j = 0; j < newuserfound.hobbies.length; j++) {
				for (int k = 0; k < hobbyOR.length; k++) {
					if (newuserfound.hobbies[j] == hobbyOR[k]) {
						getHobbyORlist.add(newuserfound);
					}
				}
			}
		}
		return getHobbyORlist;
	}

	/**
	 * This is a method to perform the hobby AND matching And refers to satisfy both
	 * the conditions
	 * @param hobbyAnd
	 * @return
	 */
	public ArrayList<User> getHobbyAndList(String[] hobbyAnd) {
		ArrayList<User> getHobbyAndlist = new ArrayList<User>();
		System.out.println("                 Matched Hobbies AND list               ");
		System.out.println("======================================================");
		User newuser = null;
		String hobby1 = hobbyAnd[0];
		String hobby2 = hobbyAnd[1];
		for (User currentuser : this.listOfAllUsers) {
			if ((Arrays.asList(currentuser.hobbies).contains(hobby1)
					&& (Arrays.asList(currentuser.hobbies).contains(hobby2)))) {
				newuser = currentuser;
				getHobbyAndlist.add(newuser);
			}
		}

		return getHobbyAndlist;
	}

	/**
	 * This is a method to perform the food_Preference either veg or non veg
	 * of the user
	 * @param preference
	 * @return
	 */
	public ArrayList<User> getMatch_Food_Preference(String preference) {
		ArrayList<User> set = new ArrayList<User>();
		System.out.println("                 Matched Food_Preference list               ");
		System.out.println("======================================================");
		for (User currUser : listOfAllUsers) {

			if (currUser.food_Preference == preference)
				set.add(currUser);

		}

		return set;

	}

	/**
	 * This is a method to perform the favorite food
	 * of the user
	 * @param favourite_Food
	 * @return
	 */

	public ArrayList<User> getMatch_Favourite_Food(String favourite_Food) {
		ArrayList<User> set = new ArrayList<User>();
		System.out.println("                 Matched Favourite_Food list               ");
		System.out.println("======================================================");
		for (User currUser : listOfAllUsers) {

			if (currUser.primary_Food == favourite_Food)
				set.add(currUser);

		}
		return set;

	}

	/**
	 * This is a method to perform the hobby OR matching Or refers to the satisfy
	 * any one of the the condition
	 * @param foodOR
	 * @return
	 */
	public ArrayList<User> getMatchFoodOR(String[] foodOR) {
		ArrayList<User> getFoodORlist = new ArrayList<User>();
		System.out.println("                 Matched Food OR list               ");
		System.out.println("======================================================");
		for (int i = 0; i < this.listOfAllUsers.size(); i++) {
			User newuserfound = this.listOfAllUsers.get(i);
			for (int j = 0; j < newuserfound.interest_Food_Items.length; j++) {
				for (int k = 0; k < foodOR.length; k++) {
					if (newuserfound.interest_Food_Items[j] == foodOR[k]) {
						getFoodORlist.add(newuserfound);
					}
				}
			}
		}
		return getFoodORlist;
	}

	/**
	 * This is a method to perform the Hobby AND Matching And refers to satisfy the
	 * both the conditions
	 * @param foodAND
	 * @return
	 */

	public ArrayList<User> getMatchFoodAND(String[] foodAND) {
		ArrayList<User> getFoodAndlist = new ArrayList<User>();
		System.out.println("                 Matched Food AND list               ");
		System.out.println("======================================================");
		User newuser = null;
		String food1 = foodAND[0];
		String food2 = foodAND[1];
		for (User currentuser : this.listOfAllUsers) {
			if ((Arrays.asList(currentuser.interest_Food_Items).contains(food1)
					&& (Arrays.asList(currentuser.interest_Food_Items).contains(food2)))) {
				newuser = currentuser;
				getFoodAndlist.add(newuser);
			}
		}

		return getFoodAndlist;
	}

	/**
	 * This is the method to perform the Minimum salary
	 * of the user
	 * @param sal
	 * @return
	 */
	public ArrayList<User> getMatchminsalary(int sal) {
		ArrayList<User> set = new ArrayList<User>();
		System.out.println("                 Matched Min Salary list               ");
		System.out.println("======================================================");
		for (User currUser : listOfAllUsers) {

			if (currUser.salary >= sal)
				set.add(currUser);

		}
		return set;

	}

	/**
	 * This is the method to perform the Maximum salary
	 * of the user
	 * @param sal
	 * @return
	 */
	public ArrayList<User> getMatchmaxsalary(int sal) {
		ArrayList<User> set = new ArrayList<User>();
		System.out.println("                 Matched Max Salary list                  ");
		System.out.println("==========================================================");
		for (User currUser : listOfAllUsers) {

			if (currUser.salary <= sal)
				set.add(currUser);
		}
		return set;

	}

	/**
	 * This is the method to perform the Minimum age
	 * of the users
	 * @param age
	 * @return
	 */
	public ArrayList<User> getMatchminage(int age) {
		ArrayList<User> set = new ArrayList<User>();
		System.out.println("				 Matched Min Age 			             ");
		System.out.println("=========================================================");
		for (User currUser : listOfAllUsers) {

			if (currUser.age >= age)
				set.add(currUser);
		}
		return set;
	}

	/**
	 * This is the method to perform Maximum age
	 * of the users
	 * @param age
	 * @return
	 */
	public ArrayList<User> getMatchmaxage(int age) {
		ArrayList<User> set = new ArrayList<User>();
		System.out.println("               Matched Max age                          ");
		System.out.println("=========================================================");
		for (User currUser : listOfAllUsers) {

			if (currUser.age <= age)
				set.add(currUser);
		}
		return set;
	}

	/**
	 * This is the method to perform the Minimum Height
	 * of the users
	 * @param Float
	 * @return
	 */
	public ArrayList<User> getMatchminheight(float Float) {
		ArrayList<User> set = new ArrayList<User>();
		System.out.println("               Matched Min Height                         ");
		System.out.println("=========================================================");
		for (User currUser : listOfAllUsers) {

			if (currUser.height >= Float)
				set.add(currUser);
		}
		return set;
	}

	/**
	 * This is the method to perform the Maximum age
	 * of the users
	 * @param Float
	 * @return
	 */
	public ArrayList<User> getMatchmaxheight(float Float) {
		ArrayList<User> set = new ArrayList<User>();
		System.out.println("               Matched Max Height                         ");
		System.out.println("=========================================================");
		for (User currUser : listOfAllUsers) {

			if (currUser.height <= Float)
				set.add(currUser);
		}
		return set;
	}

	/**
	 * This the method to create the Minimum weight
	 * of the users
	 * @param weight
	 * @return
	 */
	public ArrayList<User> getMatchminweight(int weight) {
		ArrayList<User> set = new ArrayList<User>();
		System.out.println("               Matched Min Weight                         ");
		System.out.println("=========================================================");
		for (User currUser : listOfAllUsers) {

			if (currUser.weight <= weight)
				set.add(currUser);
		}
		return set;
	}

	/**
	 * This is method to perform the Maximum weight
	 * of the  users
	 * @param weight
	 * @return
	 */
	public ArrayList<User> getMatchmaxweight(int weight) {
		ArrayList<User> set = new ArrayList<User>();
		System.out.println("               Matched Max Weight                         ");
		System.out.println("=========================================================");
		for (User currUser : listOfAllUsers) {

			if (currUser.weight >= weight)
				set.add(currUser);
		}
		return set;
	}

	/**
	 * This is the method to perform the Range of weight
	 * of the users
	 * @param minweight
	 * @param maxweight
	 * @return
	 */
	public ArrayList<User> getMatchrangeofweight(int minweight, int maxweight) {
		ArrayList<User> set = new ArrayList<User>();
		System.out.println("               Matched Range of  Weight                         ");
		System.out.println("=========================================================");
		for (User currUser : listOfAllUsers) {

			if (currUser.weight > minweight && currUser.weight < maxweight) {
				set.add(currUser);
			}
		}
		return set;
	}

	/**
	 * This is the method to perform the Range of age
	 * of the users
	 * @param minage
	 * @param maxage
	 * @return
	 */
	public ArrayList<User> getMatchrangeofage(int minage, int maxage) {
		ArrayList<User> set = new ArrayList<User>();
		System.out.println("               Matched Range of  age                          ");
		System.out.println("=========================================================");
		for (User currUser : listOfAllUsers) {

			if (currUser.age > minage && currUser.age < maxage) {
				set.add(currUser);
			}
		}
		return set;
	}

	/**
	 * This is the method to perform the Range of height
	 * of the users
	 * @param minheight
	 * @param maxheight
	 * @return
	 */
	public ArrayList<User> getMatchrangeofheight(Float minheight, Float maxheight) {
		ArrayList<User> set = new ArrayList<User>();
		System.out.println("               Matched Range of  height                          ");
		System.out.println("=========================================================");
		for (User currUser : listOfAllUsers) {

			if (currUser.height > minheight && currUser.height < maxheight) {
				set.add(currUser);
			}
		}
		return set;
	}

	/**
	 * This is the method to perform the Favorite places
	 * of the user
	 * @param favourite_place
	 * @return
	 */
	public ArrayList<User> getMatchfavplace(String favourite_place) {
		ArrayList<User> set = new ArrayList<User>();
		System.out.println("               Matched Favorite place                   ");
		System.out.println("=========================================================");
		for (User currUser : listOfAllUsers) {

			if (currUser.favourite_place == favourite_place) {
				set.add(currUser);
			}
		}
		return set;
	}

	/**
	 * This is the method to perform the Favorite places Or 
	 * which means it need to satisfy the any one of the condition
	 * @param fav_places
	 * @param FavplacesOR
	 * @return
	 */
	public ArrayList<User> getMatchFavplacesOR(String[] FavplacesOR) {

		ArrayList<User> getFoodAndlist = new ArrayList<User>();
		System.out.println("                 Matched FavPlace Or list               ");
		System.out.println("======================================================");
		User newuser = null;
		String Favplaces1 = FavplacesOR[0];
		String Favplaces2 = FavplacesOR[1];
		for (User currentuser : this.listOfAllUsers) {
			if ((Arrays.asList(currentuser.fav_places).contains(Favplaces1)
					&& (Arrays.asList(currentuser.fav_places).contains(Favplaces2)))) {
				newuser = currentuser;
				getFoodAndlist.add(newuser);
			}

		}
		return getFoodAndlist;
	}

	/**
	 * This is the method to implement the gender of users
	 * @param gen
	 * @return
	 */
	public ArrayList<User> getMatchGender(Gender gen) {
		ArrayList<User> set = new ArrayList<User>();
		System.out.println("                 Matched Gender list               ");
		System.out.println("======================================================");
		for (User currUser : listOfAllUsers) {

			if (currUser.gender_type == gen)
				set.add(currUser);

		}

		return set;
	}

	/**
	 * This is the method to implement the FAV Places And
	 * Which means it should satisfy both the conditions
	 * @param FavplacesAnd
	 * @return
	 */
	public ArrayList<User> getMatchFavplacesAnd(String[] FavplacesAnd) {

		ArrayList<User> getFoodAndlist = new ArrayList<User>();
		System.out.println("                 Matched FavPlace And list               ");
		System.out.println("======================================================");
		User newuser = null;
		String Favplaces1 = FavplacesAnd[0];
		String Favplaces2 = FavplacesAnd[1];
		for (User currentuser : this.listOfAllUsers) {
			if ((Arrays.asList(currentuser.fav_places).contains(Favplaces1)
					&& (Arrays.asList(currentuser.fav_places).contains(Favplaces2)))) {
				newuser = currentuser;
				getFoodAndlist.add(newuser);

			}
		}
		return getFoodAndlist;
	}

	/**
	 * This is the method to implement the Marital_status of the people 
	 * @param m1
	 * @return
	 */
	public ArrayList<User> getMatchMaritalstatus(Maritalstatus M1) {
		ArrayList<User> set = new ArrayList<User>();
		System.out.println("                 Matched Maritalstatus list               ");
		System.out.println("======================================================");
		for (User currUser : listOfAllUsers) {

			if (currUser.status_type == M1)
				set.add(currUser);

		}

		return set;
	}

	public ArrayList<User> getMatchQualification(String qual) {
     HashSet<User> selectedSet = new HashSet<User>();
		
		// looking at all the users in UserCollection
		for(int i =0 ; i < this.listOfAllUsers.size() ;  i++) {
			 
		User currentUser = listOfAllUsers.get(i);
		Qualification[] currUserQualifications = currentUser.qual;
		for (int j=0; j<currUserQualifications.length; j++) {
			
			Qualification currQualification = currUserQualifications[j];

			if (currQualification.course_name.contains(qual)) {
				selectedSet.add(currentUser);
			} else if (currQualification.course_branch.contains(qual)) {
				selectedSet.add(currentUser);
			} else if (currQualification.college.contains(qual)) {
				selectedSet.add(currentUser);
			} else if (currQualification.marks.contains(qual)) {
				selectedSet.add(currentUser);
			} else if (currQualification. year_of_passedout.contains(qual)) {
				selectedSet.add(currentUser);
			}  else if (currQualification. duration.contains(qual)) {
				selectedSet.add(currentUser);
			} 
		}
	}
	return new ArrayList<User>(selectedSet);
	}

  }
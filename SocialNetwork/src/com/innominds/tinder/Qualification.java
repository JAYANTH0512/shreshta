package com.innominds.tinder;
/**
 * This is the implementation of Qualification of the users
 * @author jpalepally1
 */
public class Qualification {
	public String course_name;
	public String course_branch;
	public String college;
	public String    marks;
	public String    year_of_passedout;
	public String    duration;
	
	/**
	 * @param	course_name
	 * @param	course_branch
	 * @param	college
	 * @param	marks
	 * @param   year_of_passedouts
	 * @param 	duration
	 */
	public Qualification (String course_name, String course_branch, String college,String marks,String
		year_of_passedout,String duration) {
		
		this.course_name		 = course_name;
		this.course_branch 		 = course_branch;
		this.college       		 = college;
		this.marks				 = marks;
		this.year_of_passedout   = year_of_passedout;
		this.duration          	 = duration;
		
	}	
		
		public String toString() {
			
			String stringToReturn = "";

			
			stringToReturn +=  "\n";
			stringToReturn += "course_name                 :                " + this.course_name + "\n";
			stringToReturn += "course_branch               :                " + this.course_branch + "\n";
			stringToReturn += "college                     :                " + this.college + "\n";
			stringToReturn += "marks                       :                " + this.marks + "\n";
			stringToReturn += "year_of_passedouts          :                " + this.year_of_passedout + "\n";
			stringToReturn += "duration                      :              " + this.duration + "\n";
			return stringToReturn;
		}
		
	}
	
	
	
	



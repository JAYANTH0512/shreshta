package com.innominds.tinder;
/**
 * This class represents the enum of Marital status
 * @author jpalepally1
 *
 */
public enum Maritalstatus {
		MARRIED,
		SINGLE,
		NOTINTERESTED;
}

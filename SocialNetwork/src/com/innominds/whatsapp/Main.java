package com.innominds.whatsapp;
/**
 * This is the  implementation of main method  
 * @author jpalepally1
 *
 */
import java.time.LocalDateTime;

public class Main {
	public static void main(String args[]) {
		UserCollections uc = new UserCollections();
	
		uc.createNewUser("Jayanth" ,  "Jayanth@1234"  , "944646621" ,   "read");
		uc.createNewUser("kumar",  "kumar@123" , "7334555892" ,   "unread");
		uc.createNewUser("Sai"   ,  "Password123" , "8384929291" ,   "read");
		uc.createNewUser("Kishore"  ,  "Password234" , "9236171819" ,   "read");
		
		/**
		 * user collection object calling send Message method
		 * by passing the parameters
		 * @param sendphnno
		 * @param recievephnno
		 * @param msgbody
		 */
		
		System.out.println("  *  SEND MESSAGES LIST  *");
		System.out.println("  -------------------------- ");
		uc.sendmessage("8392036127" , "9347423171" , "Welcome to Innominds" , LocalDateTime.now());
		uc.sendmessage("7468907738" , "8096012327" , "Welcome to Java"        , LocalDateTime.now());
		uc.sendmessage("9829012738" , "9666704047" , "Welcome to software"            , LocalDateTime.now());
		uc.sendmessage("9763092861" , "7172738459" , "Java project"       , LocalDateTime.now());
		
		
		/**
		 * user collection object calling receive message method
		 * by passing the parameters
		 * @param sendphnno
		 * @param recievephnno
		 * @param msgbody
		 */ 
		
		System.out.println("  *  RECEIVE MESSAGES LIST  *");
		System.out.println("  ----------------------------- ");
		uc.recievemessage("7662037910" , "9347423171" , "Hi",LocalDateTime.now());
		uc.recievemessage("9703545427" , "9763021970" , "Hello",LocalDateTime.now());
		uc.recievemessage("8630173948" , "8201730927" , "Java",LocalDateTime.now());
	}
}

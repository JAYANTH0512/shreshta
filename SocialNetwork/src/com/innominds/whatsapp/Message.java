package com.innominds.whatsapp;

import java.time.LocalDateTime;
import java.util.ArrayList;

	/**
	 * To create a Message class to implement the messages
	 * by using ArrayList
	 * @author jpalepally1
	 *
	 */

	public class Message {
		ArrayList<String> messageList = new ArrayList<String>();
		private String sendphnno;
		private String recievephnno;
		private String msgbody;
		LocalDateTime timestamp; 
		
	/**
	 * To create a constructor with parameters 
	 * using this statement	
	 * @param sendmsg
	 * @param recvmsg
	 * @param msgbody
	 */
		
	public Message(String sendphnno, String recievephnno, String msgbody, LocalDateTime timestamp) {
		this.sendphnno    = sendphnno;        //using this statement
		this.recievephnno = recievephnno;     //using this statement
		this.msgbody      = msgbody;          //using this statement
	    this.timestamp    =timestamp;
		System.out.println(this);
		}
	
	/**
	 * override the default toString method of Message 
	 * so that message can be printed in human readable format
	 */
	
	@Override
	public String toString() {
		String returnToMe=" ";
		returnToMe+="sendPhnNo    :         "+this.sendphnno+      "\n";
		returnToMe+="recievePhnNo :         "+this.recievephnno+   "\n";
		returnToMe+="messagebody  :         "+this.msgbody+        "\n";
	    returnToMe+="time         :         "+this.timestamp+      "\n";
		return returnToMe;
		
	} 
}
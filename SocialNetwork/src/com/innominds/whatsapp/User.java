package com.innominds.whatsapp;
/**
 * This is the implementation of User class 
 * @author jpalepally1
 *
 */
	public class User {
		private String name;
		private String phone;
		private String password;
		private String status;
		
		/**
		 * Create a constructor User
		 * @param name
		 * @param password
		 * @param phone
		 * @param status
		 * @throws Exception
		 */
	      public  User(String name,String password,String phone,String status) throws Exception {
			
			this.name 				= name;
			this.password           = password;
			this.phone              = phone;
			this.status             = status;
			System.out.println(this);
					
			try {
				validateName(name);
			}
			   catch(NameException n) {
				  System.out.println(n);
			   }
			
			
			try {
				validatePassword(password);
			}
			 catch (PasswordException e) {
				 System.out.println(e);
		     }
			try {
				validatePhone(phone);
			 }
			catch(PhoneException p) {
				System.out.println(p);
			 }			
		}	 
	       /**
	        * This is a helper method to perform password validation
	        * @param password
	        * @throws PasswordException
	        */
		  
		    void validatePassword(String password) throws PasswordException {
		     if (password.length() < 10) {
		         throw new PasswordException("Password length  must be contain 10 characters or above");
		     }

		     boolean upperCheck = false;
		     boolean lowerCheck = false;
		     boolean digitCheck = false;
		     for (char c : password.toCharArray()) {
		         if (Character.isUpperCase(c)) // This verifies there is a Upper case letter
		         {
		             upperCheck = true;
		         }

		         if (Character.isLowerCase(c)) // This verifies there is a lower case letter
		         {
		              lowerCheck = true;
		         }
		         if (Character.isDigit(c)) // This verifies there is a digit
		         {
		               digitCheck = true; 
		         }
		     }
		      			   
		     if (!upperCheck) {
		         throw new PasswordException("Password must be an uppercase character");
		     }

		     else if (!lowerCheck) {
		         throw new PasswordException ("Password must be a lowercase character");
		     }

		     else if (!digitCheck) {
		         throw new PasswordException ("Password must  be contain a digit");
		     }
		     else
		    	 System.out.println("");		    	  		      
		 }
		   /**
		    * This is a helper method to perform name validation
		    * @param name
		    * @throws NameException
		    */
		   void validateName(String name) throws NameException{
			   if(name.length()<3 || name.length()>10){
				   throw new NameException ("Name is Invalid,please give atleast three characters");
				   
			   }			   
		   }
		   /**
		    * This is a helper method to perform phone number validation
		    * @param phone
		 * @throws Exception 
		    */
		   void validatePhone(String phone) throws PhoneException{
			   if(phone.length()<10 || phone.length()>10) {
				 throw new PhoneException("Phone number must be 10 numbers");
				  
			   }			   
		   }
		  /**
		   * This is ClassException class to perform  it is a valid or not
		   * @author jpalepally1
		   *
		   */
		   @SuppressWarnings("serial")
		static class NameException extends Exception {

			     public NameException() {
			         super("Invalid name");
			     }

			     public NameException(String message) {
			         super("Invalid name: " + message);
			     }
			 }
		   /**
		    * This is PasswordException class to perform  it is a valid or not
		    * @author jpalepally1
		    *
		    */
		@SuppressWarnings("serial")
		static class PasswordException extends Exception {

		     public PasswordException() {
		         super("Invalid password");
		     }

		     public PasswordException(String message) {
		         super("Invalid password: " + message);
		     }
		 }
		 /**
		  * This is PhoneException class to perform  it is a valid or not
		  * @author jpalepally1
		  *
		  */
		@SuppressWarnings("serial")
		static class PhoneException extends Exception {

		     public PhoneException() {
		         super("Invalid phone number");
		     }
	         public PhoneException(String message) {
	        	 super("Invalid phonenumber:"+message);
	         }
		 }		  

		/**
		 * Overriding the default toString method of User so that User can
		 * be printed in human readable format.
		 */
		  @Override
		  public String toString() {

			String stringToReturn = "";			
			
			stringToReturn    +="               Display the user details             \n";
			stringToReturn    +="=================================================="+"\n";
			stringToReturn   += "name    :                " + this.name         +   "\n";
			stringToReturn   += "Phone   :                " + this.phone        +   "\n";
			stringToReturn   += "Password:                " + this.password     +   "\n";
			stringToReturn   += "status  :                " + this.status     +   "\n";
			//stringToReturn +="==============================================" +"\n";
			
			System.out.println("\n");
			
			return stringToReturn;
		}		  
	}
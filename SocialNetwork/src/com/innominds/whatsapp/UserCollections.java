package com.innominds.whatsapp;
/**
 * This is the implementation of user collections
 * @author jpalepally1
 *
 */
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
	
	public class UserCollections {
		ArrayList<String> userList    = new ArrayList<String>();
		ArrayList<String> sendList    = new ArrayList<String>();
		ArrayList<String> recieveList = new ArrayList<String>();
		
		/**
		 * To create createNewUser method using parameters
		 * by using ArrayList and addUser
		 * @param phoneNo
		 * @param name
		 * @param password
		 */
		
		@SuppressWarnings("unchecked")
		public boolean createNewUser(String name, String phnno, String password,String status)
		{
			try
			{
				User addUser=new User(name, phnno, password, status);
				userList.addAll((Collection<? extends String>) addUser);
				return true;
			}catch(Exception e) {
			
			return false;
			
		}
	}
		
		/**
		 * To create Send message method using parameters
		 * by using ArrayList and add send message
		 * @param sendphnno
		 * @param recievephnno
		 * @param message body
		 * @param localDateTime 
		 */
		
		@SuppressWarnings("unchecked")
		public boolean sendmessage(String sendphnno, String recievephnno, String msgbody, LocalDateTime timestamp)
		{
			try
			{
				Message addsendmsg=new Message(sendphnno,recievephnno,msgbody,timestamp);
				sendList.addAll((Collection<? extends String>) addsendmsg);
				return true;
			}catch(Exception e) {
			
			return false;
			
		   }
		}
		
		/**
		 * To create Receive message method using parameters
		 * by using ArrayList and add Receive message
		 * @param send phone number
		 * @param receieve phone number
		 * @param messagebody
		 */
		
		@SuppressWarnings("unchecked")
		public boolean recievemessage(String sendphnno, String recievephnno, String msgbody,LocalDateTime timestamp)
		{
			try
			{
				Message addrecievemsg=new Message(sendphnno,recievephnno,msgbody,timestamp);
				sendList.addAll((Collection<? extends String>) addrecievemsg);
				return true;
			}catch(Exception e) {
			
			return false;			
		}
	}
}
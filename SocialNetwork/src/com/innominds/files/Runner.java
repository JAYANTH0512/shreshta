package com.innominds.files;
/**
 * This is the main method for the file manager class  
 * @author jpalepally1
 *
 */
public class Runner {

	public static void main(String[] args) {

		FileManager fm = new FileManager();
		String inputString = args[0];
		String inputString2 = args[1];
//		String inputString3 = args[2];
		// create the file
		System.out.println((fm.createFile(inputString)) ? "create succ" : "create fail");
		
		// delete the file
		System.out.println(fm.deleteFile(inputString2) ? "delete succ"  : "delete fail");
		
		// Updating the file
//		System.out.println(fm.updatefile(inputString3) ? "Update succ"  : "Update fail");
	}	
}
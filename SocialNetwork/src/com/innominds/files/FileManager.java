package com.innominds.files;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author jpalepally1
 */
public class FileManager {

	/**
	 * This is a helper method to create a new file
	 * 
	 * @param fileName
	 * @return true if file creation is success. false if failure
	 */
	public boolean createFile(String fileName) {

		Path fileNamePath = Paths.get(fileName);
		try {
			Files.createFile(fileNamePath);
			return true; // file creation successful
		} catch (IOException ioe) {
			return false; // file creation fails
		}
	}

	/**
	 * This is a helper method to delete the file given by fileName
	 * 
	 * @param fileName
	 * @return true if file deletion is successful, false if fails
	 */
	public boolean deleteFile(String fileName) {

		Path fileNamePath = Paths.get(fileName);
		try {
			if (Files.deleteIfExists(fileNamePath)) {
				return true;
			} else {
				return false; // file deletion successful
			}
		} catch (IOException ioe) {
			return false; // file deletion fails
		}

	}

//	public boolean updatefile(String fileName) {
//
//		Path fileNamePath = Paths.get(fileName);
//		try {
//			if (Files.updateIfExists(fileNamePath)) {
//				return true;
//			} else {
//				return false; // file updating successful
//			}
//		} catch (IOException ioe) {
//			return false; // file updating fails
//		}
//
//		return false;
//
//	}
}

package meanMedianMode;
/**
 * This is the Program to calculate the simple Interest
 * for the given data
 * @author jpalepally
 *
 */
//import java.util.Scanner;
//public class SimpleInterest {
//	public static void main(String[] args) {
//		float principle,rate_of_interest,time,simple_interest; 
//		@SuppressWarnings("resource")
//		Scanner sc= new Scanner(System.in);                   //Scanner class
//		System.out.println("Enter the principle Amount");
//		principle=sc.nextInt();
//		System.out.println("Enter the rate of Interest");
//		rate_of_interest=sc.nextInt();
//		System.out.println("Enter the time period");
//		time=sc.nextInt();
//		simple_interest=((principle*rate_of_interest*time)/100);
//		System.out.println("simple_interest::"+ simple_interest);
//	}

//}
public class SimpleInterest{
	public static void main(String[] args) {
		float principle,rate,time,simple_interest;
		principle= 10000;
		rate= 5;
		time=2;
		System.out.printf("The principle amount is %f\nThe interest rate is %f\n The time taken is %f ",principle,rate,time );
		simple_interest=(principle*rate*time)/100;
		System.out.println("\nSimpleInterest::"+ simple_interest);
	}
}


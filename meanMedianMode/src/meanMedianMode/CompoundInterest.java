package meanMedianMode;
/**
 * This is the program to calculate the compound 
 * Interest for  the given data
 * @author jpalepally1
 *
 */
import java.util.Scanner;
public class CompoundInterest {

	public static void main(String[] args) {
		
      float principle,rate,time,c_interest;
      @SuppressWarnings("resource")
	  Scanner sc = new Scanner(System.in);
      System.out.println("Enter the principle amount =");
      principle= sc.nextFloat();
      System.out.println("Enter the rate of interest=");
      rate= sc.nextFloat();
      System.out.println("Enter the time period=");
      time=sc.nextFloat();
      c_interest=(float) (principle*Math.pow(1.0+rate/100,time)-principle);
      System.out.println("CompoundInterest="+ c_interest);
	}

}

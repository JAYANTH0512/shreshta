package meanMedianMode;
/**
 * This is the Program to perform the Median
 * operation for the given set of Numbers 
 * @author jpalepally1
 *
 */
public class Median
{
	public static void main(String[] args) 
	{
		   int n =5;                      // Declaring And Initializing the Variables
		   double  array[]= new  double [n]; 
		   array[0]=1;
		   array[1]=2;
		   array[2]=3;
		   array[3]=4;
		   array[4]=5;
		  // array[5]=6;

		   double m=0;
		   if(n%2==1)
		   {
			   m=array[(n+1)/2-1];
		   }
		   else 
		   {
			   m=(array[n/2-1]+array[n/2])/2;
		   }
		   
		   System.out.println("Median ="+m);
	}
}

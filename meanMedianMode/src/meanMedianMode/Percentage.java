package meanMedianMode;
import java.util.Scanner;
/**
 * This is the Program to  Calculate the Percentage 
 * of the Students
 * @author jpalepally1
 *
 */
public class Percentage {
	public static void main(String[] args) {
	float percentage;
	float total_marks;
	float scored;
	@SuppressWarnings("resource")
	Scanner sc= new Scanner(System.in);
	System.out.println("Enter your Marks");
	scored = sc.nextFloat();
	System.out.println("Enter total Marks");
	total_marks = sc.nextFloat();
	
	percentage= (float)((scored/total_marks)*100);
	System.out.println("Percentage::"+percentage);	
	}

}

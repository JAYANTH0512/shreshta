package meanMedianMode;
/**
 * This is the Program to calculate the Mean 
 * operation for the given Array
 * @author jpalepally1
 *
 */
public class Mean {
	public static void main(String args[]) 
    { 
	double[] input = {10,20,30,40};  // Initializing and 
	double n=4, sum =0;
	for (int i = 0; i < n; i++) 
	{
		sum=sum+input[i];	
	}
	System.out.println("Mean="+sum/n);
    }
}

package com.innominds.flipozon;

import java.util.ArrayList;
import java.util.HashSet;
//import java.util.Iterator;

import com.innominds.flipozon.dependencies.Rating;

/**
 * This is an implementation of a class "Product"
 * to represent a product in my implementation of
 * a e-commerce application. 
 * @author jpalepally1
 */
public class Product {
	
	private      int	id_counter = 4567;
	private   	 HashSet<Product> listOfProducts = new HashSet<Product>();
	
	private      String 	id;           // Representation of Product id
	private      String	 	name;         // Representation of Product Name
	private      float		price;        // Representation of Product Price
	private      String		description;  // Representation of Product Description 
	
	private ArrayList<Rating>	ratings;  // Rating array contains Rating objects
	
	// basic constructor
	public Product(String name, String description, float price) throws Exception {
		
	// verify input data, if the input is wrong throw exception
		if (name.trim().length() < 2){
			throw new Exception("name is too short");
		} else if (description.trim().length() < 10) {
			throw new Exception("description is too short");
		} else if (price < 1) {
			throw new Exception("price is too low or negative");
		} else {
			// initializing instance things
			this.id          = ++id_counter + "";
			this.name        = name;
			this.description = description;
			this.price       = price;
			this.ratings     = new ArrayList<Rating>();
			
			// adding newly created instance to static listOfProducts
			listOfProducts.add(this);
		}
	}

	/**
	 * This is a override of to* in human readable form.
	 */
	@Override
	public String toString() {
		String stringToReturn = "This is a product object, details below:\n";
		
		stringToReturn += " id:           " + this.id + 			"\n";
		stringToReturn += " name:         " + this.name + 			"\n";
		stringToReturn += " description:  " + this.description +	"\n";
		stringToReturn += " price:        " + this.price + 			"\n";
		stringToReturn += "-------------------------------------------";
		
		return stringToReturn;
	}
	/**
	 * Getter for the name property
	 * @return Name of the product as String
	 */
     public String getname() {
     return this.name;
     }
     
     /**
      * Getter for the id property
      * @return id of the product as String
      */
     
     	public String id() {
     		return this.id;
     	}
    
    /**
     * Getter for the Description  
     * @return Description of the product as String
     */
     
     	public String description() {
    		return this.description;
     	}

     	public int getId_counter() {
     		return id_counter;
     	}

     	public HashSet<Product> getListOfProducts() {
     		return listOfProducts;
     	}

     	public String getId() {
     		return id;
     	}

     	public String getName() {
     		return name;
     	}

     	public float getPrice() {
     		return price;
     	}

     	public String getDescription() {
     		return description;
     	}

     	public ArrayList<Rating> getRatings() {
     		return ratings;
     	}
   
  }	
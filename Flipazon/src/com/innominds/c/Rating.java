package com.innominds.flipozon.dependencies;

/**
 * This is the implementation of Rating class
 * This represents an individual rating by a customer
 * @author jpalepally1
 */
public class Rating
   {
	 String 	customer_id;	// This stands for the ID of the customer
	 byte 		rating_number;	// Represents the rating given by customer (1-5)
	 String 	rating_text;	// Represents the textual review
	 
 public Rating()// Creation of Rating method
   {
   }
 
 // Creation of a constructor for Rating
 public Rating(String customer_id, byte rating_number, String rating_text)throws Exception 
   {
   int rating_numberlength = 5;
	// Verify the input data 
		 if (rating_numberlength<0) {
			throw new Exception("Given Rating is Invalid");
		}else if (rating_numberlength>5) {
			throw new Exception("Given Rating is Invalid");
		}else if(rating_text.length()> 0 ||  rating_text .length()< 100) {
			throw new Exception("Given Review is Invalid");
		}
		 else {
				this.customer_id=customer_id;	
				this.rating_number=rating_number;
				this.rating_text=rating_text;
			  }
		
   		}	
    }
   

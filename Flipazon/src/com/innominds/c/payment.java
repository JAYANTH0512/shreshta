package com.innominds.flipazon.p;
/**
 * This is the implementation of Payment class
 * 
 * @author jpalepally
 *
 */
public class payment {
private 	    int   	id;
private			String  modeofpayment;
private			String  status;
			
	
	public payment(int id, String modeofpayment, String status) {
		 this.setId(id);
		 this.setModeofpayment(modeofpayment);
		 this.setStatus(status);
		}


	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}


	/**
	 * @return the modeofpayment
	 */
	public String getModeofpayment() {
		return modeofpayment;
	}


	/**
	 * @param modeofpayment the modeofpayment to set
	 */
	public void setModeofpayment(String modeofpayment) {
		this.modeofpayment = modeofpayment;
	}


	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}


	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
}

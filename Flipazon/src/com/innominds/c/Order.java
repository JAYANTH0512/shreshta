package com.innominds.flipazon.o;
/**
 * This is the implementation of order class
 * @author jpalepally1
 *
 */
public class Order {
	
	int       id;
    float     discount;
    String    status;
    
    /**
     * Creating a Constructor
     * @param id
     * @param discount
     * @param status
     */
      Order(int id, float discount,String status) {
    	 this.id = id;
    	 this.discount = discount;
    	 this.status = status;
     }	
    
    }
    


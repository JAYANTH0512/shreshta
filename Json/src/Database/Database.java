package Database;

//import java.io.IOException;

import org.json.simple.JSONObject;
import Operations.DdlCreateTable;
import Operations.DdlDeleteTable;
import Operations.DmlAddRecord;
/**
 * This is the Main method to implement the database
 * and call the methods from Create and delete classes 
 * @author jpalepally1
 *
 */

public class Database {
	public static void main(String[] args)throws Exception {
		//Create the DDLCreatetable class object
		DdlCreateTable createtableobj = new DdlCreateTable();
		System.out.println(createtableobj.createTable("Jai")?"Created success":"Creation failed");
		//Create the DDLDeletetable class object
		DdlDeleteTable deletetable = new DdlDeleteTable();
		System.out.println(deletetable.deleteTable("Jayanth")?"Deletion success":"Deletion failed");
		// Create the DDLaddData class object
		DmlAddRecord addrecordobj =  new DmlAddRecord();
		JSONObject jobj = new JSONObject();
		addrecordobj.addData("Jai",jobj);
	}

}

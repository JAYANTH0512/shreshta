package Operations;
/**
 * This is the class to implement the create table
 * @author jpalepally1
 */
import java.io.File;
import java.io.IOException;

public class DdlCreateTable {
	/**
	 * Method to create table 
	 * @param tablename
	 * @return true if file created else return false
	 */

	public boolean createTable(String tablename) {
		String tablenames = tablename;
		File file = new File("C:\\Users\\jpalepally\\eclipse-workspace\\Json\\src\\" + tablenames + ".Json");
		try {
			if (file.createNewFile()) {
				System.out.println("filename:" + file.getName());
				return true;
			} else {
				System.out.println("file already exists");
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

}

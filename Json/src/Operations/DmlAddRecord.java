package Operations;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * This is a class to implement the add record class to add data to the json
 * file
 * 
 * @author jpalepally1
 *
 */
public class DmlAddRecord {
	/**
	 * This is a helper method to add data to the file
	 * 
	 * @param tablename table name entered by user in main method
	 * @param jobj JSON object created to write json string into the file
	 * @return
	 * @throws IOException
	 */

	// @SuppressWarnings("unchecked")
	@SuppressWarnings("unchecked")
	public boolean addData(String tablename, JSONObject jobj) throws IOException {
		String tablenames = tablename;
		File file = new File("C:\\Users\\jpalepally\\eclipse-workspace\\Json\\src\\" + tablenames + ".Json");
		JSONObject empobj = new JSONObject(); // create the employee json object
		empobj.put("firstName", "Jai");
		empobj.put("age", "23");
		empobj.put("designation", "IT employee");
		empobj.put("location", "Hyderabad");
//		empobj.remove("age", "23");
		JSONArray listofhobbies = new JSONArray(); // create the employee hobbies json array
		listofhobbies.add("cricket");
		listofhobbies.add("Music");
		empobj.put("hobbies", listofhobbies);

		JSONObject userObject = new JSONObject();
		userObject.put("employee", empobj);
		// create the file writer object
		FileWriter filewriter = new FileWriter(file);
		filewriter.write(empobj.toJSONString());
		filewriter.flush();
		filewriter.close(); // close the file writer
		System.out.println("data added");
		// read the data from the file
		Scanner reader = new Scanner(file);
		while (reader.hasNextLine()) {
			String date = reader.nextLine();
			System.out.println(date);
		}
		reader.close();
		return true;

	}

}
